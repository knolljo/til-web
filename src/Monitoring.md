# Monitoring

## Uptime

- statping-ng
- vigil
- uptime-kuma
- gatus

## Metrics

- prometheus
- influxdb
- telegraf
- grafana